<img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/2867233/favicon.png?width=200" width="200" />

## tipplebot / firmware
tipplebox is a Raspberry Pi based cocktail making machine. It is built primarily in PHP with a modern docker container based architecture.

**tipplebot is not yet at a usable stage, but feel free to have a look around and monitor our progress.**

### Features planned
- Ingredients management
- Recipe management
- Drink ordering

### Setup procedure
1. Create an account at [resin.io](https://resin.io).
2. Setup an "Starter" app named "tipplebot".
3. Add a device, creating the OS image, writing it to the SD card and booting your Raspbery Pi.
4. From within the checkout directory, add the resin origin `git remote add resin <USERNAME>@git.resin.io:<USERNAME>/<APPNAME>.git`

### Dev environment
```bash
# Bring up dev environment
docker-compose -f docker-compose-dev/yml up

# Run PHP/Composer commands
docker exec -it php php composer.phar install
docker exec -it php php bin/console cache:clear
```

### Assets
- TODO: Handle them better
```bash
# Compile once
yarn encore dev

# Watch for changes and recompile
yarn encore dev --watch

# On deploy we create a production build of assets
yarn encore production
```