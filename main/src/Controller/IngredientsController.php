<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IngredientsController extends AbstractController
{
    /**
     * @Route("/ingredients", name="ingredients")
     */
    public function ingredients()
    {
        return $this->render('ingredients.html.twig', [
            'ingredients' => $this->getDoctrine()->getRepository(Ingredient::class)->findAll(),
        ]);
    }

    /**
     * @Route("/ingredients/add", name="ingredients_add")
     */
    public function ingredientsAdd(Request $request)
    {
        return $this->handleAddOrEdit($request, new Ingredient());
    }

    /**
     * @Route("/ingredients/edit/{ingredientId}", name="ingredients_edit")
     */
    public function ingredientsEdit(Request $request, int $ingredientId)
    {
        // Find the ingredient
        $ingredient = $this->getDoctrine()->getRepository(Ingredient::class)->find($ingredientId);
        if (is_null($ingredient)){
            $this->addFlash('error', 'Sorry, we couldn\'t find the ingredient you wanted.');
            return $this->redirectToRoute('ingredients');
        }

        return $this->handleAddOrEdit($request, $ingredient);
    }

    private function handleAddOrEdit(Request $request, Ingredient $ingredient){
        // Create form
        $form = $this->createForm(IngredientType::class, $ingredient);

        // Handle form submission
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Save the entity
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ingredient);
            $entityManager->flush();

            $this->addFlash('success', 'Your ingredient, '.$ingredient->getName().', has been saved.');
            return $this->redirectToRoute('ingredients');
        }

        return $this->render('ingredients-add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}