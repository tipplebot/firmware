<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    /**
     * @Route("/", name="menu")
     */
    public function number()
    {
        $number = random_int(0, 100);

        return $this->render('menu.html.twig', [
            'number' => $number,
        ]);
    }
}