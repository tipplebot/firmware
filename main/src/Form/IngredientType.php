<?php

namespace App\Form;

use App\Entity\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('type', ChoiceType::class, [
                'choices'  => Ingredient::TYPES,
                'expanded' => true,
                'required' => true,
            ])
            ->add('viscosity', IntegerType::class, [
                'attr' => [
                    'min' => 0,
                    'max' => 10,
                ],
                'required' => true,
            ])
            ->add('abv', PercentType::class, [
                'label' => 'ABV %',
                'attr' => [
                    'min' => 0,
                    'max' => 10,
                ],
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Save Ingredient'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => Ingredient::class,
            ]
        );
    }
}