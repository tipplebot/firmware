<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientRepository")
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    const TYPE_OPTIC = 1;
    const TYPE_PUMPED = 2;
    const TYPES = [
        'Optic' => self::TYPE_OPTIC,
        'Pumped' => self::TYPE_PUMPED,
    ];

    /**
     * @ORM\Column(type="smallint")
     */
    private $viscosity;

    /**
     * @ORM\Column(type="smallint")
     */
    private $abv;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function getTypeAsString(): string
    {
        return array_flip(self::TYPES)[$this->type];
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getViscosity(): ?int
    {
        return $this->viscosity;
    }

    public function setViscosity(int $viscosity): self
    {
        $this->viscosity = $viscosity;

        return $this;
    }

    public function getAbv(): ?int
    {
        return $this->abv;
    }

    public function setAbv(int $abv): self
    {
        $this->abv = $abv;

        return $this;
    }
}
