/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// tabler-ui stuff
require('tabler-ui/dist/assets/js/require.min.js');
const core = require('tabler-ui/dist/assets/js/core.js');
require('tabler-ui/dist/assets/css/dashboard.css');
require('tabler-ui/dist/assets/js/dashboard.js');
require('tabler-ui/dist/assets/plugins/input-mask/plugin.js');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

const $ = require('jquery');