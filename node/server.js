'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
    res.send('Hello, I am the tipplebot hardware interaction API');
});

// Setup the motor-har
var motorHat = require('motor-hat')({
    address: 0x60,
    steppers: [
        {
            W1: 'M1',
            W2: 'M2'
        }
    ]
}).init();

var glassMotor = motorHat.steppers[0];

glassMotor.setSpeed({rpm:5});

// Move the glass
app.get('/move-glass/:direction/:steps', (req, res) => {
    // Check we are happy with the direction
    if (req.params.direction === 'left' || req.params.direction === 'right'){
        var direction = req.params.direction;
        var motorDirection = direction === 'right' ? 'fwd' : 'back';
    } else {
        res.status(400).send('Invalid direction supplied.');
        return;
    }
    // Check we are happy with the direction
    if (!isNaN(req.params.steps) && req.params.steps > 0){
        var steps = req.params.steps;
    } else {
        res.status(400).send('Invalid steps supplied.');
        return;
    }

    console.log('Starting move '+direction+' ('+motorDirection+') by '+steps+' steps...');

    var stepResponse = glassMotor.stepSync(motorDirection, steps);

    console.log('Moved ok', stepResponse);

    glassMotor.release((err) => !err && console.log("IT'S FREE!!"));

    console.log('Finished ok');

    res.send('Moved glass '+direction+'...');
});

// Get the app listening...
app.listen(PORT, HOST);
console.log('Running on http://'+HOST+':'+PORT);
